<?php
/*
Plugin Name:          Test Bitbucket Release Asset
Plugin URI:           https://bitbucket.org/afragen/test-bitbucket-ra
Description:          This plugin is used for testing functionality of Bitbucket updating from a release asset.
Version:              0.1.0
Author:               Andy Fragen
License:              MIT
BitBucket Plugin URI: https://bitbucket.org/afragen/test-bitbucket-ra
Requires at least:    5.2
Requires PHP:         5.6
Release Asset:        true
*/
